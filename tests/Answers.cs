using System.Threading.Tasks;
using Npgsql;
using Shouldly;

namespace PostgresSamples
{
    public static class Answers
    {
        // 40001: could not serialize access due to concurrent update
        // 40P01: deadlock detected

        public static async Task CheckAnswerConcurrentSelect(this Task task)
        {
            await task;
        }

        public static async Task CheckAnswerConcurrentSelectWithTransaction(this Task task)
        {
            await task;
        }

        public static async Task CheckAnswerConcurrentSelectForUpdate(this Task task)
        {
            var errorCode = "40P01";
            await CheckPostgresExceptionCode(task, errorCode);
        }
        
        public static async Task CheckAnswerConcurrentRepeatableReadUpdate(this Task task)
        {
            var errorCode = "40001";
            await CheckPostgresExceptionCode(task, errorCode);
        }

        public static async Task CheckAnswerConcurrentRepeatableReadUpdateLock(this Task task)
        {
            await task;
        }

        public static async Task CheckAnswerConcurrentSelectSeparateRequests(this Task task)
        {
            var errorCode = "40P01";
            await CheckPostgresExceptionCode(task, errorCode);
        }

        public static async Task CheckAnswerSerializationAnomaly(this Task task)
        {
            var errorCode = "40001";
            await CheckPostgresExceptionCode(task, errorCode);
        }

        private static async Task CheckPostgresExceptionCode(Task task, string errorCode)
        {
            var ex = await task.ShouldThrowAsync<PostgresException>();

            ex.Severity.ShouldBe("ERROR");
            ex.SqlState.ShouldBe(errorCode);
        }
    }
}